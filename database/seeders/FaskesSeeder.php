<?php

namespace Database\Seeders;

//library
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

//models
use App\Models\FaskesModel;

class FaskesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        FaskesModel::create([
            'id'                        => 1,
            'm_provinsi_id'             => 13,
            'm_daerah_id'               => 221,
            'm_kecamatan_id'            => 3213,
            'm_desa_id'                 => 41071,
            'kode'                      => 'AA',
            'nama'                      => 'Inova Medika Solusindo',
            'alamat'                    => 'Jl. Soekarno Hatta, Bandung, Jawa Barat',
            'email'                     => 'Info@inovamedika.com',
            'default_currency_id'       => 1,
            'default_currency_position' => 'prefix',
        ]);
    }
}
