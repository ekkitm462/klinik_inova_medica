<?php

namespace Database\Seeders;

//library
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Str;

//models
use App\Models\RoleModel;
use App\Models\PrivilegesModel;

class ModulesRolesPrivilegesSeeder extends Seeder
{
    private $privId = 1;
    private $rpId = 1;
    private $modulesPrivileges = array();
    private $moduleIds = [
       'Home' => 1,
       'Dashboard' => 2,
       //master data
       'Administrasi' => 3,
       'Billing' => 4,
       'Provinsi' => 5,
       'Daerah' => 6,
       'Kecamatan' => 7,
       'Desa' => 8,
       'Faskes' => 9,
       'Pembiayaan' => 10,
       'Pasien' => 11,
       'Perawatan' => 12,
       'Roles' => 13,
       'Users'  => 14,
       'Data Karyawan' => 15,
       //layanan
       'Labor' => 16,
       'Obat' => 17, 
       'Penunjang' => 18,
       'Pengajuan Labor'=> 19,
       'Pengajuan Obat'=> 20,
       'Pengajuan Penunjang'=> 21,
       'Pemeriksaan'=> 22,
       //pembayaran
       'Checkout'=> 23,
    ];
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $modules = array();
        foreach($this->moduleIds as $name => $id) {
            $modules[] = [
                'id' => $id,
                'nama' => $name
            ];
        }
        DB::table('m_modules')->insertOrIgnore($modules);

        /**
         * PRIVILEGES TABLE INSERT
         * START
         */
        $privileges = array();

        /**
         * Home
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Home'], $privileges, [
            'homer' => 'Lihat Menu Home',
        ]);

        /**
         * Dashboard 
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Dashboard'], $privileges, [
            'dashboardr' => 'Lihat Dashboard',
        ]);

        /**
         * Administrasi
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Administrasi'], $privileges, [
            'administrasic' => 'Tambah Administrasi',
            'administrasir' => 'Lihat Administrasi',
            'administrasiu' => 'Update Administrasi',
            'administrasid' => 'Hapus Administrasi',
        ]);
        
        /**
         * Billing
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Billing'], $privileges, [
            'billingc' => 'Tambah Billing',
            'billingr' => 'Lihat Billing',
            'billingu' => 'Update Billing',
            'billingd' => 'Hapus Billing',
        ]);
        
        /**
         * Provinsi
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Provinsi'], $privileges, [
            'provinsic' => 'Tambah Provinsi',
            'provinsir' => 'Lihat Provinsi',
            'provinsiu' => 'Update Provinsi',
            'provinsid' => 'Hapus Provinsi',
        ]);
        
        /**
         * Daerah
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Daerah'], $privileges, [
            'daerahc' => 'Tambah Daerah',
            'daerahr' => 'Lihat Daerah',
            'daerahu' => 'Update Daerah',
            'daerahd' => 'Hapus Daerah',
        ]);
        
        /**
         * Kecamatan
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Kecamatan'], $privileges, [
            'kecamatanc' => 'Tambah Kecamatan',
            'kecamatanr' => 'Lihat Kecamatan',
            'kecamatanu' => 'Update Kecamatan',
            'kecamatand' => 'Hapus Kecamatan',
        ]);
        
        /**
         * Desa
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Desa'], $privileges, [
            'desac' => 'Tambah Desa',
            'desar' => 'Lihat Desa',
            'desau' => 'Update Desa',
            'desad' => 'Hapus Desa',
        ]);
        
        /**
         * Faskes
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Faskes'], $privileges, [
            'faskesc' => 'Tambah Faskes',
            'faskesr' => 'Lihat Faskes',
            'faskesu' => 'Update Faskes',
            'faskesd' => 'Hapus Faskes',
        ]);
        
        /**
         * Pembiayaan
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Pembiayaan'], $privileges, [
            'pembiayaanc' => 'Tambah Pembiayaan',
            'pembiayaanr' => 'Lihat Pembiayaan',
            'pembiayaanu' => 'Update Pembiayaan',
            'pembiayaand' => 'Hapus Pembiayaan',
        ]);
        
        /**
         * Pasien
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Pasien'], $privileges, [
            'pasienc' => 'Tambah Pasien',
            'pasienr' => 'Lihat Pasien',
            'pasienu' => 'Update Pasien',
            'pasiend' => 'Hapus Pasien',
        ]);
        
        /**
         * Perawatan
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Perawatan'], $privileges, [
            'perawatanc' => 'Tambah Perawatan',
            'perawatanr' => 'Lihat Perawatan',
            'perawatanu' => 'Update Perawatan',
            'perawatand' => 'Hapus Perawatan',
        ]);
        
        /**
         * Roles
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Roles'], $privileges, [
            'rolesc' => 'Tambah Roles',
            'rolesr' => 'Lihat Roles',
            'rolesu' => 'Update Roles',
            'rolesd' => 'Hapus Roles',
        ]);
        
        /**
         * Users
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Users'], $privileges, [
            'usersc' => 'Tambah Users',
            'usersr' => 'Lihat Users',
            'usersu' => 'Update Users',
            'usersd' => 'Hapus Users',
        ]);
        
        /**
         * Data Karyawan
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Data Karyawan'], $privileges, [
            'karyawanc' => 'Tambah Data Karyawan',
            'karyawanr' => 'Lihat Data Karyawan',
            'karyawanu' => 'Update Data Karyawan',
            'karyawand' => 'Hapus Data Karyawan',
        ]);
        
        /**
         * Labor
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Labor'], $privileges, [
            'laborc' => 'Tambah Labor',
            'laborr' => 'Lihat Labor',
            'laboru' => 'Update Labor',
            'labord' => 'Hapus Labor',
        ]);
        
        /**
         * Obat
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Obat'], $privileges, [
            'obatc' => 'Tambah Obat',
            'obatr' => 'Lihat Obat',
            'obatu' => 'Update Obat',
            'obatd' => 'Hapus Obat',
        ]);
        
        /**
         * Penunjang
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Penunjang'], $privileges, [
            'penunjangc' => 'Tambah Penunjang',
            'penunjangr' => 'Lihat Penunjang',
            'penunjangu' => 'Update Penunjang',
            'penunjangd' => 'Hapus Penunjang',
        ]);
        
        /**
         * Pengajuan Labor
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Pengajuan Labor'], $privileges, [
            'pengajuanlaborc' => 'Tambah Pengajuan Labor',
            'pengajuanlaborr' => 'Lihat Pengajuan Labor',
            'pengajuanlaboru' => 'Update Pengajuan Labor',
            'pengajuanlabord' => 'Hapus Pengajuan Labor',
        ]);
        
        /**
         * Pengajuan Obat
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Pengajuan Obat'], $privileges, [
            'pengajuanobatc' => 'Tambah Pengajuan Obat',
            'pengajuanobatr' => 'Lihat Pengajuan Obat',
            'pengajuanobatu' => 'Update Pengajuan Obat',
            'pengajuanobatd' => 'Hapus Pengajuan Obat',
        ]);
        
        /**
         * Pengajuan Penunjang
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Pengajuan Penunjang'], $privileges, [
            'pengajuanpenunjangc' => 'Tambah Pengajuan Penunjang',
            'pengajuanpenunjangr' => 'Lihat Pengajuan Penunjang',
            'pengajuanpenunjangu' => 'Update Pengajuan Penunjang',
            'pengajuanpenunjangd' => 'Hapus Pengajuan Penunjang',
        ]);
        
        /**
         * Pemeriksaan
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Pemeriksaan'], $privileges, [
            'pemeriksaanc' => 'Tambah Pemeriksaan',
            'pemeriksaanr' => 'Lihat Pemeriksaan',
            'pemeriksaanu' => 'Update Pemeriksaan',
            'pemeriksaand' => 'Hapus Pemeriksaan',
        ]);
        
        /**
         * Checkout
         */
        $privileges = $this->shapePrivilegeData($this->moduleIds['Checkout'], $privileges, [
            'Checkoutc' => 'Tambah Checkout',
            'Checkoutr' => 'Lihat Checkout',
            'Checkoutu' => 'Update Checkout',
            'Checkoutd' => 'Hapus Checkout',
        ]);
        
        DB::table('m_privileges')->insertOrIgnore($privileges);

        $superAdminPrivileges = [
            'modules' => [
                'Home',
                'Administrasi',
                'Billing',
                'Provinsi',
                'Daerah',
                'Kecamatan',
                'Desa',
                'Faskes',
                'Pembiayaan',
                'Pasien',
                'Perawatan',
                'Roles',
                'Users',
                'Data Karyawan',
                'Labor',
                'Obat', 
                'Penunjang',
                'Pengajuan Labor',
                'Pengajuan Obat',
                'Pengajuan Penunjang',
                'Pemeriksaan',
                'Checkout',
            ]
        ];
        $AdminFaskesPrivileges = [
            'modules' => [
                'Home',
                'Administrasi',
                'Billing',
                'Pembiayaan',
                'Pasien',
                'Perawatan',
                'Data Karyawan',
                'Labor',
                'Obat', 
                'Penunjang',
                'Pengajuan Labor',
                'Pengajuan Obat',
                'Pengajuan Penunjang',
                'Pemeriksaan',
                'Checkout',
            ],
        ];
        $this->givePrivileges($superAdminPrivileges, 'superadmin');
        $this->givePrivileges($AdminFaskesPrivileges, 'administrator');
    }
    private function shapePrivilegeData($moduleId, $privilegesArr, $data) {
        foreach($data as $kode => $nama) {
            $privilegesArr[] = [
                'id' => $this->privId++,
                'm_module_id' => $moduleId,
                'kode' => $kode,
                'nama' => $nama
            ];
        }
        $this->modulesPrivileges[$moduleId] = $data;
        return $privilegesArr;
    }

    private function givePrivileges($data, $kodeRole) {
        $modulesInput = array_key_exists('modules', $data) ? $data['modules'] : [];
        $privilegesInput = array_key_exists('privileges', $data) ? $data['privileges'] : [];
        $privilegesToGive = array();
        $roleId = RoleModel::where('kode', $kodeRole)->pluck('id')->first();
        foreach($modulesInput as $input) {
            foreach($this->modulesPrivileges[$this->moduleIds[$input]] as $privilege => $_) {
                $privilegesToGive[$privilege] = true;
            }
        }

        foreach($privilegesInput as $privilege) {
            if(Str::startsWith($privilege,'-')) {
                unset($privilegesToGive[Str::after($privilege, '-')]);
            } else {
                $privilegesToGive[$privilege] = true;
            }
        }

        foreach($privilegesToGive as $privilege => $_) {
            $privilegeId = PrivilegesModel::where('kode', $privilege)->pluck('id')->first();
            $rolePrivilege = [
                'id' => $this->rpId++,
                'm_role_id' => $roleId,
                'm_privilege_id' => $privilegeId
            ];

            DB::table('m_roles_privileges')->insertOrIgnore($rolePrivilege);
        }


    }
}
