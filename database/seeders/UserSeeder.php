<?php

namespace Database\Seeders;

//library
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insertOrIgnore([
        [
            'id'        => 1,
            'username'  => 'superadmin',
            'name'      => 'Super Admin',
            'email'     => 'super.admin@faskes.com',
            'password'  => Hash::make(12345678),
            'm_role_id' => 1,
            'phone'     => '082241698249',
            'alamat'    => 'Semarang',
            'status'    => 1,
        ], [
            'id'        => 2,
            'username'  => 'administrator',
            'name'      => 'Administrator',
            'email'     => 'admin.faskes@faskes.com',
            'password'  => Hash::make(12345678),
            'm_role_id' => 2,
            'phone'     => '082241698249',
            'alamat'    => 'Semarang',
            'status'    => 1,
            ]
        ]);
    }
}
