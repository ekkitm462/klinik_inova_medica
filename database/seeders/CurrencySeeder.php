<?php

namespace Database\Seeders;

//library
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

//models
use App\Models\CurrencyModel;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('currencies')->insertOrIgnore([
            [
                'id'                 => 1,
                'm_faskes_id'        => 1,
                'currency_name'      => 'Rupiah',
                'code'               => Str::upper('RP'),
                'symbol'             => 'Rp',
                'thousand_separator' => ',',
                'decimal_separator'  => '.',
                'exchange_rate'      => null
            ]
        ]);
    }
}
