<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_pasien', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('m_faskes_id');
            $table->unsignedBigInteger('m_provinsi_id');
            $table->unsignedBigInteger('m_daerah_id');
            $table->unsignedBigInteger('m_kecamatan_id');
            $table->unsignedBigInteger('m_desa_id');
            $table->unsignedBigInteger('m_pembiayaan_id');
            $table->string('nama');
            $table->string('no_rekam_medis');
            $table->string('nik');
            $table->string('no_kk');
            $table->string('no_bpjs');
            $table->string('nama_kk');
            $table->date('tanggal_lahir');
            $table->integer('jenis_kelamin');
            $table->string('golongan_darah');
            $table->string('pendidikan');
            $table->string('pekerjaan');
            $table->string('alamat');
            $table->string('rt');
            $table->string('rw');
            $table->integer('kodepos');
            $table->integer('kunjungan_sakit');
            $table->integer('jenis_perawatan');
            $table->integer('status');
            $table->integer('status_pembayaran');
            $table->timestamps();
            
            $table->foreign('m_faskes_id')->references('id')->on('m_faskes');
            $table->foreign('m_provinsi_id')->references('id')->on('m_provinsi');
            $table->foreign('m_daerah_id')->references('id')->on('m_daerah');
            $table->foreign('m_kecamatan_id')->references('id')->on('m_kecamatan');
            $table->foreign('m_desa_id')->references('id')->on('m_desa');
            $table->foreign('m_pembiayaan_id')->references('id')->on('m_pembiayaan');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_pasien');
    }
};
