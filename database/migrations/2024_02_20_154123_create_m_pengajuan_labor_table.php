<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_pengajuan_labor', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('m_pasien_id');
            $table->unsignedBigInteger('m_labor_id');
            $table->string('hasil_labor');
            $table->timestamps();
            
            $table->foreign('m_pasien_id')->references('id')->on('m_pasien');
            $table->foreign('m_labor_id')->references('id')->on('m_labor');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_pengajuan_labor');
    }
};
