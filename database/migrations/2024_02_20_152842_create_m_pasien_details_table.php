<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_pasien_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('m_pasien_id');
            $table->string('keluhan_utama');
            $table->string('riwayat_penyakit_sekarang');
            $table->string('riwayat_penggunaan_obat');
            $table->string('eye');
            $table->string('verbal');
            $table->string('motorik');
            $table->string('vs_gcs');
            $table->string('keadaan_umum');
            $table->string('td1');
            $table->string('td2');
            $table->string('hr');
            $table->string('rr');
            $table->string('suhu');
            $table->string('spo2');
            $table->string('vs_spo2');
            $table->string('bb');
            $table->string('tb');
            $table->string('ca1');
            $table->string('ca2');
            $table->string('si1');
            $table->string('si2');
            $table->string('rc1');
            $table->string('rc2');
            $table->string('pupil1');
            $table->string('pupil2');
            $table->string('s1-2');
            $table->string('murmur1');
            $table->string('murmur2');
            $table->string('gallop1');
            $table->string('gallop2');
            $table->string('sdv1');
            $table->string('sdv2');
            $table->string('rh1');
            $table->string('rh2');
            $table->string('wh1');
            $table->string('wh2');
            $table->string('retraksi1');
            $table->string('retraksi2');
            $table->string('kontur');
            $table->string('defans');
            $table->string('bu1');
            $table->string('bu2');
            $table->string('nt1');
            $table->string('nt2');
            $table->string('crt');
            $table->string('akral');
            $table->string('edema1');
            $table->string('edema2');
            $table->string('status_lokalis');
            $table->string('fisik_khusus');
            $table->string('advice_dokter');
            $table->string('tindakan_lanjutan');
            $table->timestamps();
            
            $table->foreign('m_pasien_id')->references('id')->on('m_pasien');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_pasien_details');
    }
};
