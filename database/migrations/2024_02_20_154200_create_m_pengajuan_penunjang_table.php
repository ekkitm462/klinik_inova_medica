<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_pengajuan_penunjang', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('m_pasien_id');
            $table->unsignedBigInteger('m_penunjang_id');
            $table->string('hasil_penunjang');
            $table->string('file');
            $table->timestamps();
            
            $table->foreign('m_pasien_id')->references('id')->on('m_pasien');
            $table->foreign('m_penunjang_id')->references('id')->on('m_penunjang');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_pengajuan_penunjang');
    }
};
