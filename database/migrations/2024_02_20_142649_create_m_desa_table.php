<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_desa', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('m_provinsi_id');
            $table->unsignedBigInteger('m_daerah_id');
            $table->unsignedBigInteger('m_kecamatan_id');
            $table->string('nama');
            $table->foreignId('created_by')->nullable()->constrained('users')->nullOnDelete();
            $table->foreignId('updated_by')->nullable()->constrained('users')->nullOnDelete();
            $table->timestamps();
            
            $table->foreign('m_provinsi_id')->references('id')->on('m_provinsi');
            $table->foreign('m_daerah_id')->references('id')->on('m_daerah');
            $table->foreign('m_kecamatan_id')->references('id')->on('m_kecamatan');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_desa');
    }
};
