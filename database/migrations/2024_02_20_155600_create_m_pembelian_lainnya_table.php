<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_pembelian_lainnya', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('m_faskes_id');
            $table->unsignedBigInteger('m_billing_id');
            $table->string('jumlah');
            $table->timestamps();
            
            $table->foreign('m_faskes_id')->references('id')->on('m_faskes');
            $table->foreign('m_billing_id')->references('id')->on('m_billing');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_pembelian_lainnya');
    }
};
