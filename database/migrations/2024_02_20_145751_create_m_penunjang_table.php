<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_penunjang', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('m_faskes_id');
            $table->string('nama');
            $table->integer('stok_awal');
            $table->integer('stok_akhir');
            $table->string('expired');
            $table->string('harga');
            $table->unsignedBigInteger('m_pembiayaan_id');
            $table->integer('status');
            $table->timestamps();
            
            $table->foreign('m_faskes_id')->references('id')->on('m_faskes');
            $table->foreign('m_pembiayaan_id')->references('id')->on('m_pembiayaan');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_penunjang');
    }
};
