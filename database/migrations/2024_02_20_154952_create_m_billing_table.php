<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('m_billing', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('m_faskes_id');
            $table->unsignedBigInteger('m_pembiayaan_id');
            $table->string('nama');
            $table->integer('harga');
            $table->timestamps();
            
            $table->foreign('m_faskes_id')->references('id')->on('m_faskes');
            $table->foreign('m_pembiayaan_id')->references('id')->on('m_pembiayaan');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('m_billing');
    }
};
