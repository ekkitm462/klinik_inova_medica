<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

//model
use App\Models\User;
use App\Models\RoleModel;

class UsersController extends Controller
{
    private $modelName = 'Users';
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::all();

        return view('master_data.users.index', [
            "users" => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('master_data.users.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $data = $request->all();
        $data['status'] = 1;
        $data['password']   = Hash::make($request->password);
        User::create($data);
        return redirect('users')->withSuccess('Data ' . $this->modelName . ' berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data['users'] = User::findOrFail($id);
        return view('master_data.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $users = User::findOrFail($id);
        $data = $request->all();
        $users->update($data);
        return redirect('users')->withSuccess('Data ' . $this->modelName . ' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        User::destroy($id);

        return redirect('users')->withSuccess('Data ' . $this->modelName . ' berhasil dihapus');
    }
}
