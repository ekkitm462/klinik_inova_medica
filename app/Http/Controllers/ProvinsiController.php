<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//model
use App\Models\ProvinsiModel;

class ProvinsiController extends Controller
{
    private $modelName = 'Provinsi';
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $provinsi = ProvinsiModel::all();

        return view('master_data.wilayah.provinsi.index', [
            "provinsi" => $provinsi,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('master_data.wilayah.provinsi.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $data = $request->all();
        ProvinsiModel::create($data);
        return redirect('provinsi')->withSuccess('Data ' . $this->modelName . ' berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data['provinsi'] = ProvinsiModel::findOrFail($id);
        return view('master_data.wilayah.provinsi.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $provinsi = ProvinsiModel::findOrFail($id);
        $data = $request->all();
        $provinsi->update($data);
        return redirect('provinsi')->withSuccess('Data ' . $this->modelName . ' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        ProvinsiModel::destroy($id);

        return redirect('provinsi')->withSuccess('Data ' . $this->modelName . ' berhasil dihapus');
    }
}
