<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//model
use App\Models\ProvinsiModel;
use App\Models\DaerahModel;
use App\Models\KecamatanModel;
use App\Models\DesaModel;

class DaerahController extends Controller
{

    public function __construct()
    {
        //
    }

    public function getAllProvinsi()
    {
        $provinsi = ProvinsiModel::all();
        $data = [
            'error_code'    => 0,
            'data'          => $provinsi,
        ];
        echo json_encode($data);
    }

    public function getDaerah($idMprovinsi = null)
    {
        $daerah = DaerahModel::where('m_provinsi_id', $idMprovinsi)->get();
        $data = [
            'error_code'    => 0,
            'data'          => $daerah,
        ];
        echo json_encode($data);
    }

    public function getKecamatan($idMdaerah = null)
    {
        $kecamatan = KecamatanModel::where('m_daerah_id', $idMdaerah)->get();
        $data = [
            'error_code'    => 0,
            'data'          => $kecamatan,
        ];
        echo json_encode($data);
    }

    public function getDesa($idMkecamatan = null)
    {
        $desa = DesaModel::where('m_kecamatan_id', $idMkecamatan)->get();
        $data = [
            'error_code'    => 0,
            'data'          => $desa,
        ];
        echo json_encode($data);
    }
}
