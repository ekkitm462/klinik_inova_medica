<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//model
use App\Models\ObatModel;
class ObatController extends Controller
{
    private $modelName = 'Obat';
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $obat = ObatModel::all();

        return view('master_data.obat.index', [
            "obat" => $obat,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('master_data.obat.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $data = $request->all();
        $data['m_faskes_id']    = 1;
        $data['status']    = 1;
        ObatModel::create($data);
        return redirect('obat')->withSuccess('Data ' . $this->modelName . ' berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data['obat'] = ObatModel::findOrFail($id);
        return view('master_data.obat.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $obat = ObatModel::findOrFail($id);
        $data = $request->all();
        $obat->update($data);
        return redirect('obat')->withSuccess('Data ' . $this->modelName . ' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        ObatModel::destroy($id);

        return redirect('obat')->withSuccess('Data ' . $this->modelName . ' berhasil dihapus');
    }
}
