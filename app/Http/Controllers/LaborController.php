<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//model
use App\Models\LaborModel;

class LaborController extends Controller
{
    private $modelName = 'Labor';
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $labor = LaborModel::all();

        return view('master_data.labor.index', [
            "labor" => $labor,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('master_data.labor.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $data = $request->all();
        $data['m_faskes_id']    = 1;
        $data['status']    = 1;
        LaborModel::create($data);
        return redirect('labor')->withSuccess('Data ' . $this->modelName . ' berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data['labor'] = LaborModel::findOrFail($id);
        return view('master_data.labor.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $labor = LaborModel::findOrFail($id);
        $data = $request->all();
        $labor->update($data);
        return redirect('labor')->withSuccess('Data ' . $this->modelName . ' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        LaborModel::destroy($id);

        return redirect('labor')->withSuccess('Data ' . $this->modelName . ' berhasil dihapus');
    }
}
