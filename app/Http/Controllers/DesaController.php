<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//model
use App\Models\DesaModel;

class DesaController extends Controller
{
    private $modelName = 'Desa';
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $desa = DesaModel::all();

        return view('master_data.wilayah.desa.index', [
            "desa" => $desa,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('master_data.wilayah.desa.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $data = $request->all();
        DesaModel::create($data);
        return redirect('desa')->withSuccess('Data ' . $this->modelName . ' berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data['desa'] = DesaModel::findOrFail($id);
        return view('master_data.wilayah.desa.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $desa = DesaModel::findOrFail($id);
        $data = $request->all();
        $desa->update($data);
        return redirect('desa')->withSuccess('Data ' . $this->modelName . ' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        DesaModel::destroy($id);

        return redirect('desa')->withSuccess('Data ' . $this->modelName . ' berhasil dihapus');
    }
}
