<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//model
use App\Models\PasienModel;

class PasienController extends Controller
{
    private $modelName = 'Pasien';
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pasien = PasienModel::all();

        return view('layanan.pasien.index', [
            "pasien" => $pasien,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('layanan.pasien.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $pasien = PasienModel::latest()->first();
        $data = $request->all();
        $no_urut = $pasien->no_rekam_medis??'0';
        $lastNumber = $no_urut + 1;
        $data['m_faskes_id'] = 1;
        $data['no_rekam_medis'] = str_pad($lastNumber, 4, '0', STR_PAD_LEFT);
        $data['status'] = 1;
        $data['kunjungan_sakit'] = 1;
        $data['status_pembayaran'] = 0;
        PasienModel::create($data);
        return redirect('pasien')->withSuccess('Data ' . $this->modelName . ' berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data['pasien'] = PasienModel::findOrFail($id);
        return view('layanan.pasien.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $pasien = PasienModel::findOrFail($id);
        $data = $request->all();
        $pasien->update($data);
        return redirect('pasien')->withSuccess('Data ' . $this->modelName . ' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        PasienModel::destroy($id);

        return redirect('pasien')->withSuccess('Data ' . $this->modelName . ' berhasil dihapus');
    }
}
