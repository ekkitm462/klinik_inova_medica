<?php

namespace App\Http\Controllers\DataTables;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

//model
use App\Models\PasienModel;

class PasienDataTables extends Controller
{

    public function __construct()
    {
        //
    }

    function getTabelPasien($idMDesa = null){

        $pasien = PasienModel::where('m_desa_id', $idMDesa)->latest()->get();

        $i = 1;
        foreach ($pasien as $row)
        {
            $btnEdit = "<a href='".route('pasien.edit', ['id' => $row->id])."'><i class='fas fa-pencil-alt ms-text-primary'></i></a>";
            // $btnDelete = "<a href='".route('pasien.destroy', ['id' => $row->id])."' onclick=\"event.preventDefault(); document.getElementById('delete-form-{$row->id}').submit();\"><i class='far fa-trash-alt ms-text-danger'></i></a> ";

            // // Ini harus disimpan dalam variabel atau echo untuk menampilkan form di HTML.
            // $deleteForm = "<form id='delete-form-{$row->id}' action='".route('pasien.destroy', ['id' => $row->id])."' method='POST' style='display: none;'>";
            // $deleteForm .= csrf_field();
            // $deleteForm .= "</form>";

            // $btn = $btnEdit.' '.$btnDelete.$deleteForm;

            $tbody      = [];
            $tbody[]    = $i++;
            $tbody[]    = $row->nama;
            $tbody[]    = $row->nik;
            $tbody[]    = $row->no_rekam_medis;
            $tbody[]    = $row->no_bpjs;
            if ($row->jenis_perawatan == 1) {
                $tbody[]    = "Rawat Jalan";
            } else {
                $tbody[]    = "Rawat Inap";
            }
            if ($row->status == 1) {
                $tbody[]    = "<span class='badge badge-pill badge-warning'>Sedang Diperiksa Dokter</span>";
            } elseif ($row->status == 2) {
                $tbody[]    = "<span class='badge badge-pill badge-warning'>Sedang Pemeriksaan Labor</span>";
            } elseif ($row->status == 3) {
                $tbody[]    = "<span class='badge badge-pill badge-warning'>Sedang Pengajuan Rawat Inap</span>";
            } elseif ($row->status == 4) {
                $tbody[]    = "<span class='badge badge-pill badge-warning'>Sedang Perawatan Inap</span>";
            } elseif ($row->status == 5) {
                $tbody[]    = "<span class='badge badge-pill badge-secondary'>Sedang Menunggu Obat</span>";
            } elseif ($row->status == 6) {
                $tbody[]    = "<span class='badge badge-pill badge-info'>Sedang Menunggu Pembayaran</span>";
            } elseif ($row->status == 7) {
                $tbody[]    = "<span class='badge badge-pill badge-success'>Selesai</span>";
            } elseif ($row->status == 8) {
                $tbody[]    = "<span class='badge badge-pill badge-info'>Sedang Pemeriksaan Penunjang</span>";
            }
            $tbody[]    = $btnEdit;

            $data[]     = $tbody;
        }

        // if ($pasien != null)
        if (count($pasien) > 0)
        {
            $response = [
                'data'      => $data,
            ];
            echo json_encode($response);
        }else{
            $response = [
                'data'      => '',
            ];
            echo json_encode($response);
        }
    }
}
