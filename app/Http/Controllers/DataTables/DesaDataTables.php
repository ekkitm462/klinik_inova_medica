<?php

namespace App\Http\Controllers\DataTables;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

//model
use App\Models\DesaModel;

class DesaDataTables extends Controller
{

    public function __construct()
    {
        //
    }

    function getTabelDesa($idMKecamatan = null){

        $desa = DesaModel::where('m_kecamatan_id', $idMKecamatan)->latest()->get();

        $i = 1;
        foreach ($desa as $row)
        {
            $btnEdit = "<a href='".route('desa.edit', ['id' => $row->id])."'><i class='fas fa-pencil-alt ms-text-primary'></i></a>";
            // $btnDelete = "<a href='".route('desa.destroy', ['id' => $row->id])."' onclick=\"event.preventDefault(); document.getElementById('delete-form-{$row->id}').submit();\"><i class='far fa-trash-alt ms-text-danger'></i></a> ";

            // // Ini harus disimpan dalam variabel atau echo untuk menampilkan form di HTML.
            // $deleteForm = "<form id='delete-form-{$row->id}' action='".route('desa.destroy', ['id' => $row->id])."' method='POST' style='display: none;'>";
            // $deleteForm .= csrf_field();
            // $deleteForm .= "</form>";

            // $btn = $btnEdit.' '.$btnDelete.$deleteForm;

            $tbody      = [];
            $tbody[]    = $i++;
            $tbody[]    = $row->nama;
            $tbody[]    = $btnEdit;

            $data[]     = $tbody;
        }

        // if ($desa != null)
        if (count($desa) > 0)
        {
            $response = [
                'data'      => $data,
            ];
            echo json_encode($response);
        }else{
            $response = [
                'data'      => '',
            ];
            echo json_encode($response);
        }
    }
}
