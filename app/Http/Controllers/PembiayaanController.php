<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//model
use App\Models\PembiayaanModel;

class PembiayaanController extends Controller
{
    private $modelName = 'Pembiayaan';
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pembiayaan = PembiayaanModel::all();

        return view('master_data.pembiayaan.index', [
            "pembiayaan" => $pembiayaan,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('master_data.pembiayaan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $data = $request->all();
        PembiayaanModel::create($data);
        return redirect('pembiayaan')->withSuccess('Data ' . $this->modelName . ' berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data['pembiayaan'] = PembiayaanModel::findOrFail($id);
        return view('master_data.pembiayaan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $pembiayaan = PembiayaanModel::findOrFail($id);
        $data = $request->all();
        $pembiayaan->update($data);
        return redirect('pembiayaan')->withSuccess('Data ' . $this->modelName . ' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        PembiayaanModel::destroy($id);

        return redirect('pembiayaan')->withSuccess('Data ' . $this->modelName . ' berhasil dihapus');
    }
}
