<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//model
use App\Models\KecamatanModel;
class KecamatanController extends Controller
{
    private $modelName = 'Kecamatan';
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kecamatan = KecamatanModel::all();

        return view('master_data.wilayah.kecamatan.index', [
            "kecamatan" => $kecamatan,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('master_data.wilayah.kecamatan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
        ]);

        $data = $request->all();
        KecamatanModel::create($data);
        return redirect('kecamatan')->withSuccess('Data ' . $this->modelName . ' berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data['kecamatan'] = KecamatanModel::findOrFail($id);
        return view('master_data.wilayah.kecamatan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $kecamatan = KecamatanModel::findOrFail($id);
        $data = $request->all();
        $kecamatan->update($data);
        return redirect('kecamatan')->withSuccess('Data ' . $this->modelName . ' berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        KecamatanModel::destroy($id);

        return redirect('kecamatan')->withSuccess('Data ' . $this->modelName . ' berhasil dihapus');
    }
}
