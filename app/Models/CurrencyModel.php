<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrencyModel extends Model
{
    use HasFactory;
    protected $table = 'currencies';
    protected $fillable = [
        'm_faskes_id', 'currency_name', 'code', 'symbol', 'thousand_separator', 'decimal_separator', 'exchange_rate'
    ];
    
    public function faskes()
    {
        return $this->belongsTo(FaskesModel::class, 'm_faskes_id', 'id');
    }
}
