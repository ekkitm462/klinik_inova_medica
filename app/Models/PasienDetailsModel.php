<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasienDetailsModel extends Model
{
    use HasFactory;
    protected $table = 'm_pasien_details';
    protected $guarded = ['id'];
    
    public function pasien()
    {
        return $this->belongsTo(PasienModel::class, 'm_pasien_id', 'id');
    }
}
