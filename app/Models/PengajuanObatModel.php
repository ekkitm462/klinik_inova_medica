<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanObatModel extends Model
{
    use HasFactory;
    protected $table = 'm_pengajuan_obat';
    protected $guarded = ['id'];
    
    public function pasien()
    {
        return $this->belongsTo(PasienModel::class, 'm_pasien_id', 'id');
    }
    
    public function obat()
    {
        return $this->belongsTo(ObatModel::class, 'm_obat_id', 'id');
    }
}
