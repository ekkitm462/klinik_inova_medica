<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanLaborModel extends Model
{
    use HasFactory;
    protected $table = 'm_pengajuan_labor';
    protected $guarded = ['id'];
    
    public function pasien()
    {
        return $this->belongsTo(PasienModel::class, 'm_pasien_id', 'id');
    }
    
    public function labor()
    {
        return $this->belongsTo(LaborModel::class, 'm_labor_id', 'id');
    }
}
