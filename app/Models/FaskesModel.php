<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FaskesModel extends Model
{
    use HasFactory;
    protected $table = 'm_faskes';
    protected $fillable = [
        'kode', 'nama', 'alamat', 'email', 'default_currency_id', 'default_currency_position'
    ];
    public function currency() {
        return $this->belongsTo(CurrencyModel::class, 'default_currency_id', 'id');
    }
}
