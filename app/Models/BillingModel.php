<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillingModel extends Model
{
    use HasFactory;
    protected $table = 'm_billing';
    protected $fillable = [
        'm_faskes_id', 'm_pembiayaan_id', 'nama', 'harga'
    ];
    
    public function faskes()
    {
        return $this->belongsTo(FaskesModel::class, 'm_faskes_id', 'id');
    }
    
    public function pembiayaan()
    {
        return $this->belongsTo(PembiayaanModel::class, 'm_pembiayaan_id', 'id');
    }
}
