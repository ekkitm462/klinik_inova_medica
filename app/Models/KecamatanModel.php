<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KecamatanModel extends Model
{
    use HasFactory;
    protected $table = 'm_kecamatan';
    protected $fillable = [
        'm_provinsi_id', 'm_daerah_id', 'nama'
    ];
    
    public function provinsi()
    {
        return $this->belongsTo(ProvinsiModel::class, 'm_provinsi_id', 'id');
    }
    
    public function daerah()
    {
        return $this->belongsTo(DaerahModel::class, 'm_daerah_id', 'id');
    }
}
