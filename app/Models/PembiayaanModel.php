<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PembiayaanModel extends Model
{
    use HasFactory;
    protected $table = 'm_pembiayaan';
    protected $fillable = [
        'nama'
    ];
}
