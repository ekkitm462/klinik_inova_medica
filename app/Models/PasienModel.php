<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasienModel extends Model
{
    use HasFactory;
    protected $table = 'm_pasien';
    protected $guarded = ['id'];
    
    public function faskes()
    {
        return $this->belongsTo(FaskesModel::class, 'm_faskes_id', 'id');
    }
    public function perawatan()
    {
        return $this->belongsTo(PerawatanModel::class, 'm_perawatan_id', 'id');
    }
    public function pembiayaan()
    {
        return $this->belongsTo(PembiayaanModel::class, 'm_pembiayaan_id', 'id');
    }
    
    public function provinsi()
    {
        return $this->belongsTo(ProvinsiModel::class, 'm_provinsi_id', 'id');
    }
    
    public function daerah()
    {
        return $this->belongsTo(DaerahModel::class, 'm_daerah_id', 'id');
    }
    
    public function kecamatan()
    {
        return $this->belongsTo(KecamatanModel::class, 'm_kecamatan_id', 'id');
    }
    
    public function desa()
    {
        return $this->belongsTo(DesaModel::class, 'm_desa_id', 'id');
    }
}
