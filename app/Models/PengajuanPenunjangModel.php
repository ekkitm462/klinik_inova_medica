<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengajuanPenunjangModel extends Model
{
    use HasFactory;
    protected $table = 'm_pengajuan_penunjang';
    protected $guarded = ['id'];
    
    public function pasien()
    {
        return $this->belongsTo(PasienModel::class, 'm_pasien_id', 'id');
    }
    
    public function penunjang()
    {
        return $this->belongsTo(PenunjangModel::class, 'm_penunjang_id', 'id');
    }
}
