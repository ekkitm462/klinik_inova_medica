<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenunjangModel extends Model
{
    use HasFactory;
    protected $table = 'm_penunjang';
    protected $fillable = [
        'm_faskes_id', 'nama', 'stok_awal', 'stok_akhir', 'expired', 
        'harga', 'm_pembiayaan_id', 'status'
    ];
    
    public function faskes()
    {
        return $this->belongsTo(FaskesModel::class, 'm_faskes_id', 'id');
    }
    public function pembiayaan()
    {
        return $this->belongsTo(PembiayaanModel::class, 'm_pembiayaan_id', 'id');
    }
}
