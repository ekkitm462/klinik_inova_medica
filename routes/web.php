<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', 'Auth\LoginController')->name('/');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');

    //master data
        //data karyawan
        Route::prefix('users')->group(function(){
            Route::get('/', 'UsersController@index')->name('users');
            Route::get('/create', 'UsersController@create')->name('users.create');
            Route::post('/', 'UsersController@store')->name('users.store');
            Route::get('/edit/{id}', 'UsersController@edit')->name('users.edit');
            Route::put('/{id}', 'UsersController@update')->name('users.update');
            Route::post('/delete/{id}', 'UsersController@destroy')->name('users.destroy');
        });

        //data provinsi
        Route::prefix('provinsi')->group(function(){
            Route::get('/', 'ProvinsiController@index')->name('provinsi');
            Route::get('/create', 'ProvinsiController@create')->name('provinsi.create');
            Route::post('/', 'ProvinsiController@store')->name('provinsi.store');
            Route::get('/edit/{id}', 'ProvinsiController@edit')->name('provinsi.edit');
            Route::put('/{id}', 'ProvinsiController@update')->name('provinsi.update');
            Route::post('/delete/{id}', 'ProvinsiController@destroy')->name('provinsi.destroy');
        });

        //data daerah
        Route::prefix('daerah')->group(function(){
            Route::get('/', 'DaerahController@index')->name('daerah');
            Route::get('/create', 'DaerahController@create')->name('daerah.create');
            Route::post('/', 'DaerahController@store')->name('daerah.store');
            Route::get('/edit/{id}', 'DaerahController@edit')->name('daerah.edit');
            Route::put('/{id}', 'DaerahController@update')->name('daerah.update');
            Route::post('/delete/{id}', 'DaerahController@destroy')->name('daerah.destroy');
        });

        //data kecamatan
        Route::prefix('kecamatan')->group(function(){
            Route::get('/', 'KecamatanController@index')->name('kecamatan');
            Route::get('/create', 'KecamatanController@create')->name('kecamatan.create');
            Route::post('/', 'KecamatanController@store')->name('kecamatan.store');
            Route::get('/edit/{id}', 'KecamatanController@edit')->name('kecamatan.edit');
            Route::put('/{id}', 'KecamatanController@update')->name('kecamatan.update');
            Route::post('/delete/{id}', 'KecamatanController@destroy')->name('kecamatan.destroy');
        });

        //data desa
        Route::prefix('desa')->group(function(){
            Route::get('/', 'DesaController@index')->name('desa');
            Route::get('/create', 'DesaController@create')->name('desa.create');
            Route::post('/', 'DesaController@store')->name('desa.store');
            Route::get('/edit/{id}', 'DesaController@edit')->name('desa.edit');
            Route::put('/{id}', 'DesaController@update')->name('desa.update');
            Route::post('/delete/{id}', 'DesaController@destroy')->name('desa.destroy');
        });

        //data users
        Route::prefix('users')->group(function(){
            Route::get('/', 'UsersController@index')->name('users');
            Route::get('/create', 'UsersController@create')->name('users.create');
            Route::post('/', 'UsersController@store')->name('users.store');
            Route::get('/edit/{id}', 'UsersController@edit')->name('users.edit');
            Route::put('/{id}', 'UsersController@update')->name('users.update');
            Route::post('/delete/{id}', 'UsersController@destroy')->name('users.destroy');
        });

        //data roles
        Route::prefix('roles')->group(function(){
            Route::get('/', 'RolesController@index')->name('roles');
            Route::get('/create', 'RolesController@create')->name('roles.create');
            Route::post('/', 'RolesController@store')->name('roles.store');
            Route::get('/edit/{id}', 'RolesController@edit')->name('roles.edit');
            Route::put('/{id}', 'RolesController@update')->name('roles.update');
            Route::post('/delete/{id}', 'RolesController@destroy')->name('roles.destroy');
        });

        //data administrasi
        Route::prefix('administrasi')->group(function(){
            Route::get('/', 'AdministrasiController@index')->name('administrasi');
            Route::get('/create', 'AdministrasiController@create')->name('administrasi.create');
            Route::post('/', 'AdministrasiController@store')->name('administrasi.store');
            Route::get('/edit/{id}', 'AdministrasiController@edit')->name('administrasi.edit');
            Route::put('/{id}', 'AdministrasiController@update')->name('administrasi.update');
            Route::post('/delete/{id}', 'AdministrasiController@destroy')->name('administrasi.destroy');
        });

        //data billing
        Route::prefix('billing')->group(function(){
            Route::get('/', 'BillingController@index')->name('billing');
            Route::get('/create', 'BillingController@create')->name('billing.create');
            Route::post('/', 'BillingController@store')->name('billing.store');
            Route::get('/edit/{id}', 'BillingController@edit')->name('billing.edit');
            Route::put('/{id}', 'BillingController@update')->name('billing.update');
            Route::post('/delete/{id}', 'BillingController@destroy')->name('billing.destroy');
        });

        //data currency
        Route::prefix('currency')->group(function(){
            Route::get('/', 'CurrencyController@index')->name('currency');
            Route::get('/create', 'CurrencyController@create')->name('currency.create');
            Route::post('/', 'CurrencyController@store')->name('currency.store');
            Route::get('/edit/{id}', 'CurrencyController@edit')->name('currency.edit');
            Route::put('/{id}', 'CurrencyController@update')->name('currency.update');
            Route::post('/delete/{id}', 'CurrencyController@destroy')->name('currency.destroy');
        });

        //data faskes
        Route::prefix('faskes')->group(function(){
            Route::get('/', 'FaskesController@index')->name('faskes');
            Route::get('/create', 'FaskesController@create')->name('faskes.create');
            Route::post('/', 'FaskesController@store')->name('faskes.store');
            Route::get('/edit/{id}', 'FaskesController@edit')->name('faskes.edit');
            Route::put('/{id}', 'FaskesController@update')->name('faskes.update');
            Route::post('/delete/{id}', 'FaskesController@destroy')->name('faskes.destroy');
        });

        //data pembiayaan
        Route::prefix('pembiayaan')->group(function(){
            Route::get('/', 'PembiayaanController@index')->name('pembiayaan');
            Route::get('/create', 'PembiayaanController@create')->name('pembiayaan.create');
            Route::post('/', 'PembiayaanController@store')->name('pembiayaan.store');
            Route::get('/edit/{id}', 'PembiayaanController@edit')->name('pembiayaan.edit');
            Route::put('/{id}', 'PembiayaanController@update')->name('pembiayaan.update');
            Route::post('/delete/{id}', 'PembiayaanController@destroy')->name('pembiayaan.destroy');
        });
        
        //data perawatan
        Route::prefix('perawatan')->group(function(){
            Route::get('/', 'PerawatanController@index')->name('perawatan');
            Route::get('/create', 'PerawatanController@create')->name('perawatan.create');
            Route::post('/', 'PerawatanController@store')->name('perawatan.store');
            Route::get('/edit/{id}', 'PerawatanController@edit')->name('perawatan.edit');
            Route::put('/{id}', 'PerawatanController@update')->name('perawatan.update');
            Route::post('/delete/{id}', 'PerawatanController@destroy')->name('perawatan.destroy');
        });
        
    //layanan
        //data pasien
        Route::prefix('pasien')->group(function(){
            Route::get('/', 'PasienController@index')->name('pasien');
            Route::get('/create', 'PasienController@create')->name('pasien.create');
            Route::post('/', 'PasienController@store')->name('pasien.store');
            Route::get('/edit/{id}', 'PasienController@edit')->name('pasien.edit');
            Route::put('/{id}', 'PasienController@update')->name('pasien.update');
            Route::post('/delete/{id}', 'PasienController@destroy')->name('pasien.destroy');
        });
        
        //data rawat-jalan
        Route::prefix('rawat-jalan')->group(function(){
            Route::get('/', 'RajalController@index')->name('rawat-jalan');
            Route::get('/create', 'RajalController@create')->name('rawat-jalan.create');
            Route::post('/', 'RajalController@store')->name('rawat-jalan.store');
            Route::get('/edit/{id}', 'RajalController@edit')->name('rawat-jalan.edit');
            Route::put('/{id}', 'RajalController@update')->name('rawat-jalan.update');
            Route::post('/delete/{id}', 'RajalController@destroy')->name('rawat-jalan.destroy');
        });

        //data obat
        Route::prefix('obat')->group(function(){
            Route::get('/', 'ObatController@index')->name('obat');
            Route::get('/create', 'ObatController@create')->name('obat.create');
            Route::post('/', 'ObatController@store')->name('obat.store');
            Route::get('/edit/{id}', 'ObatController@edit')->name('obat.edit');
            Route::put('/{id}', 'ObatController@update')->name('obat.update');
            Route::post('/delete/{id}', 'ObatController@destroy')->name('obat.destroy');
        });

        //data pengajuan-obat
        Route::prefix('pengajuan-obat')->group(function(){
            Route::get('/', 'PengajuanObatController@index')->name('pengajuan-obat');
            Route::get('/create', 'PengajuanObatController@create')->name('pengajuan-obat.create');
            Route::post('/', 'PengajuanObatController@store')->name('pengajuan-obat.store');
            Route::get('/edit/{id}', 'PengajuanObatController@edit')->name('pengajuan-obat.edit');
            Route::put('/{id}', 'PengajuanObatController@update')->name('pengajuan-obat.update');
            Route::post('/delete/{id}', 'PengajuanObatController@destroy')->name('pengajuan-obat.destroy');
        });

        //data penunjang
        Route::prefix('penunjang')->group(function(){
            Route::get('/', 'PenunjangController@index')->name('penunjang');
            Route::get('/create', 'PenunjangController@create')->name('penunjang.create');
            Route::post('/', 'PenunjangController@store')->name('penunjang.store');
            Route::get('/edit/{id}', 'PenunjangController@edit')->name('penunjang.edit');
            Route::put('/{id}', 'PenunjangController@update')->name('penunjang.update');
            Route::post('/delete/{id}', 'PenunjangController@destroy')->name('penunjang.destroy');
        });

        //data pengajuan-penunjang
        Route::prefix('pengajuan-penunjang')->group(function(){
            Route::get('/', 'PengajuanPenunjangController@index')->name('pengajuan-penunjang');
            Route::get('/create', 'PengajuanPenunjangController@create')->name('pengajuan-penunjang.create');
            Route::post('/', 'PengajuanPenunjangController@store')->name('pengajuan-penunjang.store');
            Route::get('/edit/{id}', 'PengajuanPenunjangController@edit')->name('pengajuan-penunjang.edit');
            Route::put('/{id}', 'PengajuanPenunjangController@update')->name('pengajuan-penunjang.update');
            Route::post('/delete/{id}', 'PengajuanPenunjangController@destroy')->name('pengajuan-penunjang.destroy');
        });

        //data labor
        Route::prefix('labor')->group(function(){
            Route::get('/', 'LaborController@index')->name('labor');
            Route::get('/create', 'LaborController@create')->name('labor.create');
            Route::post('/', 'LaborController@store')->name('labor.store');
            Route::get('/edit/{id}', 'LaborController@edit')->name('labor.edit');
            Route::put('/{id}', 'LaborController@update')->name('labor.update');
            Route::post('/delete/{id}', 'LaborController@destroy')->name('labor.destroy');
        });

        //data pengajuan-labor
        Route::prefix('pengajuan-labor')->group(function(){
            Route::get('/', 'PengajuanLaborController@index')->name('pengajuan-labor');
            Route::get('/create', 'PengajuanLaborController@create')->name('pengajuan-labor.create');
            Route::post('/', 'PengajuanLaborController@store')->name('pengajuan-labor.store');
            Route::get('/edit/{id}', 'PengajuanLaborController@edit')->name('pengajuan-labor.edit');
            Route::put('/{id}', 'PengajuanLaborController@update')->name('pengajuan-labor.update');
            Route::post('/delete/{id}', 'PengajuanLaborController@destroy')->name('pengajuan-labor.destroy');
        });

    //checkout
        //data checkout
        Route::prefix('checkout')->group(function(){
            Route::get('/', 'CheckoutController@index')->name('checkout');
            Route::get('/create', 'CheckoutController@create')->name('checkout.create');
            Route::post('/', 'CheckoutController@store')->name('checkout.store');
            Route::get('/edit/{id}', 'CheckoutController@edit')->name('checkout.edit');
            Route::put('/{id}', 'CheckoutController@update')->name('checkout.update');
            Route::post('/delete/{id}', 'CheckoutController@destroy')->name('checkout.destroy');
        });

        //data pembayaran-lunas
        Route::prefix('pembayaran-lunas')->group(function(){
            Route::get('/', 'CheckoutController@pembayaranLunas')->name('pembayaran-lunas');
        });
});
