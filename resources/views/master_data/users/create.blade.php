@extends('layouts.app')

@section('title', 'Tambah Data Users')

@section('css-library')
    {{-- Tempat Ngoding Meletakkan css library --}}
@endsection

@section('css-custom')
    {{-- Tempat Ngoding Meletakkan css custom --}}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                   <div class="iq-header-title">
                      <h4 class="card-title">{{ __('Tambah Data Users') }}</h4>
                   </div>
                </div>
                <div class="iq-card-body">
                    <div class="btn-group">
                        <a href="{{ route('users') }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Kembali">
                            Kembali
                        </a>
                    </div>
                    <div class="btn-group">&nbsp;</div>
                   <form action="{{ route('users.store') }}" method="POST">
                    @csrf
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Users</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nama" placeholder="Nama Users" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Password</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="password" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Role</label>
                            <select id="select-role" class="form-control Select2 form-control-lg" name="m_role_id" aria-label="Default select example">
                                <option value="">Pilih Role</option>
                                @foreach(\App\Models\RoleModel::where('nama', '!=', 'Super Admin')->get() as $role)
                                    <option value="{{ $role->id }}">{{ $role->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary me-1 mt-2">Submit</button>
                        <button type="reset" class="btn iq-bg-danger mt-2">Cancel</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-library')
    {{-- Tempat Ngoding Meletakkan js library --}}
@endsection

@section('js-custom')
    {{-- Tempat Ngoding Meletakkan js custom --}}
@endsection