@extends('layouts.app')

@section('title', 'Edit Data Users')

@section('css-library')
    {{-- Tempat Ngoding Meletakkan css library --}}
@endsection

@section('css-custom')
    {{-- Tempat Ngoding Meletakkan css custom --}}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                   <div class="iq-header-title">
                      <h4 class="card-title">{{ __('Edit Data Users') }}</h4>
                   </div>
                </div>
                <div class="iq-card-body">
                    <div class="btn-group">
                        <a href="{{ route('users') }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Tambah Unit">
                            Kembali
                        </a>
                    </div>
                    <div class="btn-group">&nbsp;</div>
                   <form action="{{ route('users.update', $users) }}" method="POST">
                    @csrf
                    @method('put')
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Users</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nama" value="{{ old('name', $users->name) }}" required>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Role</label>
                            <select id="select-role" class="form-control Select2 form-control-lg" name="m_role_id" aria-label="Default select example">
                                <option value="{{ $users->m_role_id }}" selected>{{ $users->role->nama }}</option>
                                <option value="">Pilih Role</option>
                                @foreach(\App\Models\RoleModel::where('nama', '!=', 'Super Admin')->get() as $role)
                                <option value="{{ $role->id }}">{{ $role->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select class="form-control form-control-lg" id="exampleFormControlSelect1"  name="status" required>
                                <option value="{{ $users->status }}" selected>
                                    @if($users->status == 1)
                                        Aktif
                                    @elseif ($users->status == 2)
                                        Tidak Aktif
                                    @endif
                                </option>
                                <option value="">Pilih Status</option>
                                <option value="1">Aktif</option>
                                <option value="2">Tidak Aktif</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary me-1 mt-2">Submit</button>
                        <button type="reset" class="btn iq-bg-danger mt-2">Cancel</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-library')
    {{-- Tempat Ngoding Meletakkan js library --}}
@endsection

@section('js-custom')
    {{-- Tempat Ngoding Meletakkan js custom --}}
@endsection