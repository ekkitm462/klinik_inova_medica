@extends('layouts.app')

@section('title', 'Edit Data Kecamatan')

@section('css-library')
    {{-- Tempat Ngoding Meletakkan css library --}}
@endsection

@section('css-custom')
    {{-- Tempat Ngoding Meletakkan css custom --}}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                   <div class="iq-header-title">
                      <h4 class="card-title">{{ __('Edit Data Kecamatan') }}</h4>
                   </div>
                </div>
                <div class="iq-card-body">
                    <div class="btn-group">
                        <a href="{{ route('kecamatan') }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Tambah Unit">
                            Kembali
                        </a>
                    </div>
                    <div class="btn-group">&nbsp;</div>
                   <form action="{{ route('kecamatan.update', $kecamatan) }}" method="POST">
                    @csrf
                    @method('put')
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Provinsi</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" value="{{ old('m_provinsi_id', $kecamatan->provinsi->nama) }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Daerah</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" value="{{ old('m_daerah_id', $kecamatan->daerah->nama) }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Kecamatan</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nama" value="{{ old('nama', $kecamatan->nama) }}" required>
                        </div>
                        <button type="submit" class="btn btn-primary me-1 mt-2">Submit</button>
                        <button type="reset" class="btn iq-bg-danger mt-2">Cancel</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-library')
    {{-- Tempat Ngoding Meletakkan js library --}}
@endsection

@section('js-custom')
    {{-- Tempat Ngoding Meletakkan js custom --}}
@endsection