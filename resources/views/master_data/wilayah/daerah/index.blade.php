@extends('layouts.app')

@section('title', 'Data Daerah')

@section('css-library')
    {{-- Tempat Ngoding Meletakkan css library --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ url('assets/css/select2.css') }}">
@endsection

@section('css-custom')
    {{-- Tempat Ngoding Meletakkan css custom --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{ session('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                    <div class="iq-header-title">
                       <h4 class="card-title">{{ __('Data Daerah') }}</h4>
                    </div>
                 </div>
                <div class="iq-card-body">
                    
                    @if(Helpers::hasPrivilege('daerahc'))
                        <div class="btn-group">
                            <a href="{{ route('daerah.create') }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Tambah Unit"><span
                                    class="fas fa-plus"></span></a>
                        </div>
                        <div class="btn-group">&nbsp;</div>
                    @endif
                    <div class="form-group">
                        <label class="col-sm-3 col-form-label">Provinsi</label>
                        <select id="select-provinsi" class="form-control Select2 form-control-lg" name="m_provinsi_id" aria-label="Default select example">
                            <option value="">Pilihan Provinsi Sedang di Proses</option>
                        </select>
                    </div>
                    <div class="table-responsive" id="tabel-jquery">
                        <table id="isi-tabel" class="table table-striped table-bordered dt-responsive nowrap dataTable no-footer dtr-inline collapsed">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Nama</th>
                                    <th width="10%"><i class="fas fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-library')
    {{-- Tempat Ngoding Meletakkan js library --}}

    <!-- Required datatable js -->
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Buttons examples -->
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.print.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/buttons.colVis.min.js') }}"></script>

    <!-- Responsive examples -->
    <script src="{{ asset('assets/js/datatable/datatable-extension/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable/datatable-extension/responsive.bootstrap4.min.js') }}"></script>

@endsection

@section('js-custom')
    {{-- Tempat Ngoding Meletakkan js custom --}}
    <script>
        $(function() {
            $("#isi-tabel").DataTable(); // tambahin ini ki
    
            let baseUrl     = '{{ url('') }}';
            $('#tabel-jquery').hide();

            $.getJSON(baseUrl+'/api/getAllProvinsi', (result) => {
                if (result.error_code == '0') {
                    let opt = '<option value="">- Pilih -</option>';
                    $.each(result.data, function(i, item) {
                        opt += '<option value="'+item.id+'">'+item.nama+'</option>';
                    });
                    $("#select-provinsi").html(opt);
                }else{
                    let opt = '<option value="">- Tidak Bisa -</option>';
                    $("#select-provinsi").html(opt);
                }
                $("#select-provinsi").select2({
                    placeholder: "Pilih Provinsi",
                });
            });
    
            $('#select-provinsi').on('change', function() {
                    $("#isi-tabel").DataTable().clear().destroy();
                    
                    $('#tabel-jquery').show();
                    var idMProvinsi = $('#select-provinsi').val();
                    
                    $("#isi-tabel").DataTable({
                    // $("#tabel-jquery")({
                        language: {
                            emptyTable: "Tidak ada data Daerah",
                            info: "Total: _TOTAL_ Data Daerah",
                            infoEmpty: "Menampilkan 0 dari 0 Data Daerah",
                        },
                        responsive:  true,
                        autoWidth: false,
                        processing: true,
                        ajax: {
                            url: baseUrl+'/api/getDataDaerah/' +idMProvinsi,
                            method: 'POST',
                        },
                        dom: 'Bfrtip',
                        buttons: [
                            'excel'
                        ]
                    });
                });
    
        });
    </script>
@endsection