@extends('layouts.app')

@section('title', 'Tambah Data Penunjang')

@section('css-library')
    {{-- Tempat Ngoding Meletakkan css library --}}
@endsection

@section('css-custom')
    {{-- Tempat Ngoding Meletakkan css custom --}}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                   <div class="iq-header-title">
                      <h4 class="card-title">{{ __('Tambah Data Penunjang') }}</h4>
                   </div>
                </div>
                <div class="iq-card-body">
                    <div class="btn-group">
                        <a href="{{ route('penunjang') }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Kembali">
                            Kembali
                        </a>
                    </div>
                    <div class="btn-group">&nbsp;</div>
                   <form action="{{ route('penunjang.store') }}" method="POST">
                    @csrf
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Penunjang</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nama" placeholder="Nama Penunjang" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Stok Awal</label>
                            <input type="number" class="form-control my-2" id="exampleInputText1" name="stok_awal" placeholder="Stok Awal" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Stok Akhir</label>
                            <input type="number" class="form-control my-2" id="exampleInputText1" name="stok_akhir" placeholder="Stok Akhir" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Expired</label>
                            <input type="date" class="form-control my-2" id="exampleInputText1" name="expired" placeholder="Expired" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Harga</label>
                            <input type="text" class="form-control my-2" id="harga" name="harga" placeholder="Harga" required>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Jenis Pembiayaan</label>
                            <select id="select-jenis-pembiayaan" class="form-control Select2 form-control-lg" name="m_pembiayaan_id" aria-label="Default select example">
                                <option value="">Pilih Jenis Pembiayaan</option>
                                @foreach(\App\Models\PembiayaanModel::all() as $pembiayaan)
                                    <option value="{{ $pembiayaan->id }}">{{ $pembiayaan->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary me-1 mt-2">Submit</button>
                        <button type="reset" class="btn iq-bg-danger mt-2">Cancel</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-library')
    {{-- Tempat Ngoding Meletakkan js library --}}
@endsection

@section('js-custom')
    {{-- Tempat Ngoding Meletakkan js custom --}}
    <script src="{{ asset('assets/js/jquery-mask-money.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#harga').maskMoney({
                prefix:'{{ settings()->currency->symbol }}',
                thousands:'{{ settings()->currency->thousand_separator }}',
                decimal:'{{ settings()->currency->decimal_separator }}',
            });
        });
    </script>
@endsection