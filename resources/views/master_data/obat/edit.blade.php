@extends('layouts.app')

@section('title', 'Edit Data Obat')

@section('css-library')
    {{-- Tempat Ngoding Meletakkan css library --}}
@endsection

@section('css-custom')
    {{-- Tempat Ngoding Meletakkan css custom --}}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                   <div class="iq-header-title">
                      <h4 class="card-title">{{ __('Edit Data Obat') }}</h4>
                   </div>
                </div>
                <div class="iq-card-body">
                    <div class="btn-group">
                        <a href="{{ route('obat') }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Tambah Unit">
                            Kembali
                        </a>
                    </div>
                    <div class="btn-group">&nbsp;</div>
                   <form action="{{ route('obat.update', $obat) }}" method="POST">
                    @csrf
                    @method('put')
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Obat</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nama" value="{{ old('nama', $obat->nama) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Stok Awal</label>
                            <input type="number" class="form-control my-2" id="exampleInputText1" name="stok_awal" value="{{ old('stok_awal', $obat->stok_awal) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Stok Akhir</label>
                            <input type="number" class="form-control my-2" id="exampleInputText1" name="stok_akhir" value="{{ old('stok_akhir', $obat->stok_akhir) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Expired</label>
                            <input type="date" class="form-control my-2" id="exampleInputText1" name="expired" value="{{ old('expired', $obat->expired) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Harga</label>
                            <input type="text" class="form-control my-2" id="harga" name="harga" value="{{ old('harga', $obat->harga) }}" required>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Jenis Pembiayaan</label>
                            <select id="select-jenis-pembiayaan" class="form-control Select2 form-control-lg" name="m_pembiayaan_id" aria-label="Default select example">
                                <option value="{{ old('m_pembiayaan_id', $obat->m_pembiayaan_id) }}">{{ $obat->pembiayaan->nama }}</option>
                                <option value="">Pilih Jenis Pembiayaan</option>
                                @foreach(\App\Models\PembiayaanModel::all() as $pembiayaan)
                                    <option value="{{ $pembiayaan->id }}">{{ $pembiayaan->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary me-1 mt-2">Submit</button>
                        <button type="reset" class="btn iq-bg-danger mt-2">Cancel</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-library')
    {{-- Tempat Ngoding Meletakkan js library --}}
@endsection

@section('js-custom')
    {{-- Tempat Ngoding Meletakkan js custom --}}
    <script src="{{ asset('assets/js/jquery-mask-money.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#harga').maskMoney({
                prefix:'{{ settings()->currency->symbol }}',
                thousands:'{{ settings()->currency->thousand_separator }}',
                decimal:'{{ settings()->currency->decimal_separator }}',
            });
        });
    </script>
@endsection