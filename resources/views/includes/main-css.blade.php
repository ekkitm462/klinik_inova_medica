<!-- Bootstrap CSS -->
<link id="bootstrap-css" rel="stylesheet" href="{{ url('') }}/assets/css/bootstrap.min.css">
<!-- Typography CSS -->
<link rel="stylesheet" href="{{ url('') }}/assets/css/typography.css">
<!-- Style CSS -->
<link rel="stylesheet" href="{{ url('') }}/assets/css/style.css">
<!-- Style-Rtl CSS -->
<link rel="stylesheet" href="{{ url('') }}/assets/css/style-rtl.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="{{ url('') }}/assets/css/responsive.css">
 <!-- Full calendar -->
<link href='{{ url('') }}/assets/fullcalendar/core/main.css' rel='stylesheet' />
<link href='{{ url('') }}/assets/fullcalendar/daygrid/main.css' rel='stylesheet' />
<link href='{{ url('') }}/assets/fullcalendar/timegrid/main.css' rel='stylesheet' />
<link href='{{ url('') }}/assets/fullcalendar/list/main.css' rel='stylesheet' />

<link rel="stylesheet" href="{{ url('') }}/assets/css/flatpickr.min.css">
<link href="{{ url('') }}/vendors/iconic-fonts/font-awesome/css/all.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ url('') }}/assets/css/datatables.css">