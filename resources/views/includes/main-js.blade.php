<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ url('') }}/assets/js/jquery.min.js"></script>
<script src="{{ url('') }}/assets/js/popper.min.js"></script>
<script src="{{ url('') }}/assets/js/bootstrap.min.js"></script>
<!-- Appear JavaScript -->
<script src="{{ url('') }}/assets/js/jquery.appear.js"></script>
<!-- Countdown JavaScript -->
<script src="{{ url('') }}/assets/js/countdown.min.js"></script>
<!-- Counterup JavaScript -->
<script src="{{ url('') }}/assets/js/waypoints.min.js"></script>
<script src="{{ url('') }}/assets/js/jquery.counterup.min.js"></script>
<!-- Wow JavaScript -->
<script src="{{ url('') }}/assets/js/wow.min.js"></script>
<!-- Apexcharts JavaScript -->
<script src="{{ url('') }}/assets/js/apexcharts.js"></script>
<!-- Swiper Slider JavaScript -->
<script src="{{ url('') }}/assets/js/swiper-bundle.min.js"></script>
<!-- Select2 JavaScript -->
<script src="{{ url('') }}/assets/js/select2.min.js"></script>
<!-- Owl Carousel JavaScript -->
<script src="{{ url('') }}/assets/js/owl.carousel.min.js"></script>
<!-- Magnific Popup JavaScript -->
<script src="{{ url('') }}/assets/js/jquery.magnific-popup.min.js"></script>
<!-- Smooth Scrollbar JavaScript -->
<script src="{{ url('') }}/assets/js/smooth-scrollbar.js"></script>
<!-- lottie JavaScript -->
<script src="{{ url('') }}/assets/js/lottie.js"></script>
<!-- am core JavaScript -->
<script src="{{ url('') }}/assets/js/core.js"></script>
<!-- am charts JavaScript -->
<script src="{{ url('') }}/assets/js/amcharts.js"></script>
<!-- am animated JavaScript -->
<script src="{{ url('') }}/assets/js/animated.js"></script>
<!-- am kelly JavaScript -->
<script src="{{ url('') }}/assets/js/kelly.js"></script>
<!-- Flatpicker Js -->
<script src="{{ url('') }}/assets/js/flatpickr.js"></script>
<!-- Chart Custom JavaScript -->
<script src="{{ url('') }}/assets/js/chart-custom.js"></script>
<!-- Custom JavaScript -->
<script src="{{ url('') }}/assets/js/custom.js"></script>
<script src="{{ url('') }}/assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="{{ url('') }}/assets/js/datatable/datatables/datatable.custom.js"></script>