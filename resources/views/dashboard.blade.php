@extends('layouts.app')

@section('title', 'Home')

@section('css-library')
    {{-- Tempat Ngoding Meletakkan css library --}}
@endsection

@section('css-custom')
    {{-- Tempat Ngoding Meletakkan css custom --}}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{ session('success') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="iq-card">
                <div class="iq-card-body">
                    <span class="line-height-4">Selamat Datang! <b>{{ Auth::user()->name }}</b>, Anda Login sebagai {{ Auth::user()->role->nama }}</span>
                    <p class="mb-0 text-secondary line-height">Sistem Informasi Klinik PT Inovasi Medika Solusindo</p>
                </div>
            </div>
        </div>               
    </div>
@endsection

@section('js-library')
    {{-- Tempat Ngoding Meletakkan js library --}}
@endsection

@section('js-custom')
    {{-- Tempat Ngoding Meletakkan js custom --}}
@endsection