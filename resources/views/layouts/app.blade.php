
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>@yield('title') || {{ config('app.name') }}</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="{{ url('') }}/assets/images/favicon.ico" />
      
      @include('includes.main-css')
      @yield('css-library')
      @yield('css-custom')


   </head>
   <body class="sidebar-main-menu">
      <!-- loader Start -->
      <div id="loading">
         <div id="loading-center">

         </div>
      </div>
      <!-- loader END -->
      <!-- Wrapper Start -->
      <div class="wrapper">
         <!-- Sidebar  -->
         <div class="iq-sidebar">
            @include('layouts.sidebar')
         </div>
         
         <!-- Page Content  -->
            <div id="content-page" class="content-page">
                <!-- TOP Nav Bar -->
                <div class="iq-top-navbar header-top-sticky">
                    @include('layouts.header')
                </div>
                <!-- TOP Nav Bar END -->
                <div class="container-fluid">
                    @yield('content')
                
                </div>
                <!-- Footer -->
                @include('layouts.footer')
                <!-- Footer END -->
            </div>
        </div>
        <!-- Wrapper END -->
      
        <!-- Optional JavaScript -->
        @include('includes.main-js')
        @yield('js-library')
        @yield('js-custom')
    </body>
</html>
