<div class="iq-navbar-custom">
    <div class="iq-sidebar-logo">
        <div class="top-logo">
            <a href="index.html" class="logo">
            <img src="{{ url('') }}/assets/images/logo.png" class="img-fluid" alt="">
            <span>XRay</span>
            </a>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light p-0">
        <div class="iq-search-bar">
            <h4>PT Inovasi Medika Solusindo</h4>
        </div>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" href="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="ri-menu-3-line"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto navbar-list align-items-center">
                <li class="nav-item">
                    <div class="wrapper-menu">
                        <div class="main-circle"><i class="ri-more-fill"></i></div>
                        <div class="hover-circle"><i class="ri-more-2-fill"></i></div>
                    </div>
                </li>   
                <li class="nav-item">
                    <a href="javascript:void(0);" class="rtl-switch-toogle">
                        <span class="form-check form-switch">
                            <input class="form-check-input rtl-switch" type="checkbox" role="switch" id="rtl-switch">
                            <span class="rtl-toggle-tooltip ltr-tooltip">Ltr</span>
                            <span class="rtl-toggle-tooltip rtl-tooltip">Rtl</span>
                        </span>
                    </a>
                </li>   
                <li class="nav-item iq-full-screen">
                    <a href="#" class="iq-waves-effect" id="btnFullscreen"><i class="ri-fullscreen-line"></i></a>
                </li>
                <li class="nav-item">
                    <a href="#" class="search-toggle iq-waves-effect">
                        <i class="ri-notification-3-fill"></i>
                        <span class="bg-danger dots"></span>
                    </a>
                    <div class="iq-sub-dropdown">
                        <div class="iq-card shadow-none m-0">
                            <div class="iq-card-body p-0 ">
                                <div class="bg-primary p-3">
                                <h5 class="mb-0 text-white d-flex justify-content-between">All Notifications<small class="badge  badge-dark float-end pt-1">4</small></h5>
                                </div>

                                <a href="#" class="iq-sub-card" >
                                <div class="media align-items-center d-flex">
                                    <div class="">
                                        <img class="avatar-40 rounded" src="{{ url('') }}/assets/images/user/01.jpg" alt="">
                                    </div>
                                    <div class="media-body ms-3">
                                        <h6 class="mb-0 ">Emma Watson Bini</h6>
                                        <small class="float-end font-size-12">Just Now</small>
                                        <p class="mb-0">95 MB</p>
                                    </div>
                                </div>
                                </a>
                                <a href="#" class="iq-sub-card" >
                                <div class="media align-items-center d-flex">
                                    <div class="">
                                        <img class="avatar-40 rounded" src="{{ url('') }}/assets/images/user/02.jpg" alt="">
                                    </div>
                                    <div class="media-body ms-3">
                                        <h6 class="mb-0 ">New customer is join</h6>
                                        <small class="float-end font-size-12">5 days ago</small>
                                        <p class="mb-0">Jond Bini</p>
                                    </div>
                                </div>
                                </a>
                                <a href="#" class="iq-sub-card" >
                                <div class="media align-items-center d-flex">
                                    <div class="">
                                        <img class="avatar-40 rounded" src="{{ url('') }}/assets/images/user/03.jpg" alt="">
                                    </div>
                                    <div class="media-body ms-3">
                                        <h6 class="mb-0 ">Two customer is left</h6>
                                        <small class="float-end font-size-12">2 days ago</small>
                                        <p class="mb-0">Jond Bini</p>
                                    </div>
                                </div>
                                </a>
                                <a href="#" class="iq-sub-card" >
                                <div class="media align-items-center d-flex">
                                    <div class="">
                                        <img class="avatar-40 rounded" src="{{ url('') }}/assets/images/user/04.jpg" alt="">
                                    </div>
                                    <div class="media-body ms-3">
                                        <h6 class="mb-0 ">New Mail from Fenny</h6>
                                        <small class="float-end font-size-12">3 days ago</small>
                                        <p class="mb-0">Jond Bini</p>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <ul class="navbar-list">
            <li>
                <a href="#" class="search-toggle iq-waves-effect d-flex align-items-center">
                {{-- <img src="{{ url('') }}/assets/images/user/1.jpg" class="img-fluid rounded" alt="user"> --}}
                <figure class="img-profile rounded-circle avatar font-weight-bold" data-initial="{{ Auth::user()->name[0] }}"></figure>
                <div class="caption">
                    <h6 class="mb-0 line-height">{{ Auth::user()->name }}</h6>
                    <span class="font-size-12">{{ Auth::user()->role->nama }}</span>
                </div>
                </a>
                <div class="iq-sub-dropdown iq-user-dropdown">
                <div class="iq-card shadow-none m-0">
                    <div class="iq-card-body p-0 ">
                        <div class="bg-primary p-3">
                            <h5 class="mb-0 text-white line-height">Hello {{ Auth::user()->name }}</h5>
                            <span class="text-white font-size-12">{{ Auth::user()->role->nama }}</span>
                        </div>
                        <a href="profile.html" class="iq-sub-card iq-bg-primary-hover">
                            <div class="media align-items-center d-flex">
                            <div class="rounded iq-card-icon iq-bg-primary">
                                <i class="ri-file-user-line"></i>
                            </div>
                            <div class="media-body ms-3">
                                <h6 class="mb-0 ">Profile</h6>
                                <p class="mb-0 font-size-12">Lihat Profile Saya</p>
                            </div>
                            </div>
                        </a>
                        <a href="account-setting.html" class="iq-sub-card iq-bg-primary-hover">
                            <div class="media align-items-center d-flex">
                            <div class="rounded iq-card-icon iq-bg-primary">
                                <i class="ri-account-box-line"></i>
                            </div>
                            <div class="media-body ms-3">
                                <h6 class="mb-0 ">settings</h6>
                                <p class="mb-0 font-size-12">Setting Akun</p>
                            </div>
                            </div>
                        </a>
                        <div class="d-inline-block w-100 text-center p-3">
                            <a class="bg-primary iq-sign-btn" role="button" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout<i class="ri-login-box-line ms-2"></i>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </li>
        </ul>
    </nav>

</div>