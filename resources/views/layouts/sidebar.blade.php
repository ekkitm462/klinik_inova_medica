<div class="iq-sidebar-logo d-flex justify-content-between">
    <a href="{{ url('home') }}">
      <img src="{{ url('') }}/assets/images/logo.png" class="img-fluid" alt="">
      <span>XRay</span>
    </a>
    <div class="iq-menu-bt-sidebar">
          <div class="iq-menu-bt align-self-center">
             <div class="wrapper-menu">
                <div class="main-circle"><i class="ri-more-fill"></i></div>
                <div class="hover-circle"><i class="ri-more-2-fill"></i></div>
             </div>
          </div>
       </div>
 </div>
 <div id="sidebar-scrollbar">
    <nav class="iq-sidebar-menu">
       <ul class="iq-menu">
         <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Dashboard</span></li>
         @if(Helpers::hasPrivilege('homer'))
            <li>
               <a class="{{ request()->routeIs('home') ? 'c-active' : '' }}" href="{{ route('home') }}" class="iq-waves-effect"><i class="fas fa-home"></i><span>Home</span></a>
            </li>   
         @endif
         @if(Helpers::hasPrivilege('dashboardr'))
            <li>
               <a class="{{ request()->routeIs('dashboard') ? 'c-active' : '' }}" href="{{ route('dashboard') }}" class="iq-waves-effect"><i class="fas fa-home"></i><span>Dashboard</span></a>
            </li>  
         @endif
         <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Master Data</span></li>
         <li>
            <a href="javascript:void(0);" class="iq-waves-effect"><i class="fas fa-map"></i><span>Wilayah</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
            <ul class="iq-submenu">
               @if(Helpers::hasPrivilege('provinsir'))
                  <li><a class="{{ request()->routeIs('provinsi') ? 'c-active' : '' }}" href="{{ route('provinsi') }}"><i class="fas fa-map"></i>Provinsi</a></li>
               @endif
               @if(Helpers::hasPrivilege('daerahr'))
                  <li><a class="{{ request()->routeIs('daerah') ? 'c-active' : '' }}" href="{{ route('daerah') }}"><i class="fas fa-map"></i>Daerah</a></li>
               @endif
               @if(Helpers::hasPrivilege('kecamatanr'))
                  <li><a class="{{ request()->routeIs('kecamatan') ? 'c-active' : '' }}" href="{{ route('kecamatan') }}"><i class="fas fa-map"></i>Kecamatan</a></li>
               @endif
               @if(Helpers::hasPrivilege('desar'))
                  <li><a class="{{ request()->routeIs('desa') ? 'c-active' : '' }}" href="{{ route('desa') }}"><i class="fas fa-map"></i>Desa</a></li>
               @endif
            </ul>
         </li>
          
         <li>
            <a href="javascript:void(0);" class="iq-waves-effect"><i class="fas fa-user"></i><span>Users</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
            <ul class="iq-submenu">
               @if(Helpers::hasPrivilege('usersr'))
                  <li><a class="{{ request()->routeIs('users') ? 'c-active' : '' }}" href="{{ route('users') }}"><i class="fas fa-user"></i>Data Users</a></li>
               @endif
               @if(Helpers::hasPrivilege('rolesr'))
                  <li><a class="{{ request()->routeIs('roles') ? 'c-active' : '' }}" href="{{ route('roles') }}"><i class="fas fa-user"></i> Roles & Permissions</a></li>
               @endif
            </ul>
         </li>
         
         <li>
            <a href="javascript:void(0);" class="iq-waves-effect"><i class="fas fa-user"></i><span>Jenis Pembayaran</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
            <ul class="iq-submenu">
               @if(Helpers::hasPrivilege('administrasir'))
                  <li><a class="{{ request()->routeIs('administrasi') ? 'c-active' : '' }}" href="{{ route('administrasi') }}"><i class="fas fa-user"></i>Data Administrasi</a></li>
               @endif
               @if(Helpers::hasPrivilege('billingr'))
                  <li><a class="{{ request()->routeIs('billing') ? 'c-active' : '' }}" href="{{ route('billing') }}"><i class="fas fa-user"></i> Data Billing</a></li>
               @endif
            </ul>
         </li>
         
         @if(Helpers::hasPrivilege('faskesr'))
            <li>
               <a href="{{ route('faskes') }}" class="iq-waves-effect {{ request()->routeIs('faskes') ? 'c-active' : '' }}"><i class="ri-hospital-fill"></i><span>Faskes</span></a>
            </li>
         @endif
         
         @if(Helpers::hasPrivilege('pembiayaanr'))
            <li>
               <a href="{{ route('pembiayaan') }}" class="iq-waves-effect {{ request()->routeIs('pembiayaan') ? 'c-active' : '' }}"><i class="fas fa-credit-card"></i><span>Pembiayaan</span></a>
            </li>
         @endif
         
         @if(Helpers::hasPrivilege('perawatanr'))
            <li>
               <a href="{{ route('perawatan') }}" class="iq-waves-effect {{ request()->routeIs('perawatan') ? 'c-active' : '' }}"><i class="fas fa-user-md"></i><span>Perawatan</span></a>
            </li>
         @endif
          
         <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Layanan</span></li>
         <li>
            <a href="javascript:void(0);" class="iq-waves-effect"><i class="fas fa-user-md"></i><span>Pasien</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
            <ul class="iq-submenu">
               <li><a class="{{ request()->routeIs('pasien') ? 'c-active' : '' }}" href="{{ route('pasien') }}"><i class="fas fa-user-md"></i>Data Pasien</a></li>
               <li><a class="{{ request()->routeIs('pasien.create') ? 'c-active' : '' }}" href="{{ route('pasien') }}"><i class="fas fa-user-md"></i> Pendaftaran Pasien</a></li>
            </ul>
         </li>
         <li>
            <a href="javascript:void(0);" class="iq-waves-effect"><i class="fas fa-user-md"></i><span>Pemeriksaan</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
            <ul class="iq-submenu">
               <li><a class="{{ request()->routeIs('rawat-jalan') ? 'c-active' : '' }}" href="{{ route('rawat-jalan') }}"><i class="fas fa-user-md"></i>Rawat Jalan</a></li>
               <li><a class="{{ request()->routeIs('rawat-inap') ? 'c-active' : '' }}" href="#"><i class="fas fa-user-md"></i> Rawat Inap</a></li>
            </ul>
         </li>
         <li>
            <a href="javascript:void(0);" class="iq-waves-effect"><i class="fas fa-briefcase-medical"></i><span>Obat</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
            <ul class="iq-submenu">
               @if(Helpers::hasPrivilege('obatr'))
                  <li><a class="{{ request()->routeIs('obat') ? 'c-active' : '' }}" href="{{ route('obat') }}"><i class="fas fa-briefcase-medical"></i>Data Obat</a></li>
                  <li><a class="{{ request()->routeIs('pengajuan-obat') ? 'c-active' : '' }}" href="{{ route('pengajuan-obat') }}"><i class="fas fa-briefcase-medical"></i> Pengajuan Obat</a></li>
               @endif
            </ul>
         </li>
         <li>
            <a href="javascript:void(0);" class="iq-waves-effect"><i class="fas fa-briefcase-medical"></i><span>Labor</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
            <ul class="iq-submenu">
               @if(Helpers::hasPrivilege('laborr'))
                  <li><a class="{{ request()->routeIs('labor') ? 'c-active' : '' }}" href="{{ route('labor') }}"><i class="fas fa-briefcase-medical"></i>Data Labor</a></li>
                  <li><a class="{{ request()->routeIs('pengajuan-labor') ? 'c-active' : '' }}" href="{{ route('pengajuan-labor') }}"><i class="fas fa-briefcase-medical"></i> Pengajuan Labor</a></li>
               @endif
            </ul>
         </li>
         <li>
            <a href="javascript:void(0);" class="iq-waves-effect"><i class="fas fa-briefcase-medical"></i><span>Penunjang</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
            <ul class="iq-submenu">
               @if(Helpers::hasPrivilege('penunjangr'))
                  <li><a class="{{ request()->routeIs('penunjang') ? 'c-active' : '' }}" href="{{ route('penunjang') }}"><i class="fas fa-briefcase-medical"></i>Data Penunjang</a></li>
                  <li><a class="{{ request()->routeIs('pengajuan-penunjang') ? 'c-active' : '' }}" href="{{ route('pengajuan-penunjang') }}"><i class="fas fa-briefcase-medical"></i> Pengajuan Penunjang</a></li>
               @endif
            </ul>
         </li>
          
         <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Pembayaran</span></li>
         <li>
            <a href="javascript:void(0);" class="iq-waves-effect"><i class="fas fa-user-md"></i><span>Pembayaran</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
            <ul class="iq-submenu">
               @if(Helpers::hasPrivilege('checkoutr'))
                  <li><a class="{{ request()->routeIs('checkout') ? 'c-active' : '' }}" href="{{ route('checkout') }}"><i class="fas fa-user-md"></i>Pengajuan Pembayaran</a></li>
                  <li><a class="{{ request()->routeIs('pembayaran-lunas') ? 'c-active' : '' }}" href="{{ route('pembayaran-lunas') }}"><i class="fas fa-user-md"></i> Pembayaran Lunas</a></li>
               @endif
            </ul>
         </li>
       </ul>
    </nav>
    <div class="p-3"></div>
 </div>