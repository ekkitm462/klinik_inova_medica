@extends('layouts.app')

@section('title', 'Pendaftaran Pasien')

@section('css-library')
    {{-- Tempat Ngoding Meletakkan css library --}}
@endsection

@section('css-custom')
    {{-- Tempat Ngoding Meletakkan css custom --}}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                   <div class="iq-header-title">
                      <h4 class="card-title">{{ __('Pendaftaran Pasien') }}</h4>
                   </div>
                </div>
                <div class="iq-card-body">
                    <div class="btn-group">
                        <a href="{{ route('pasien') }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Kembali">
                            Kembali
                        </a>
                    </div>
                    <div class="btn-group">&nbsp;</div>
                   <form action="{{ route('pasien.store') }}" method="POST">
                    @csrf
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Jenis Perawatan</label>
                            <select id="select-jenisKel" class="form-control Select2 form-control-lg" name="jenis_perawatan" aria-label="Default select example">
                                <option value="">Pilih Jenis Perawatan</option>
                                <option value="1">Rawat Jalan</option>
                                <option value="2" disabled>Rawat Inap</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Jenis Pembiayaan</label>
                            <select id="select-jenis-pembiayaan" class="form-control Select2 form-control-lg" name="m_pembiayaan_id" aria-label="Default select example">
                                <option value="">Pilih Jenis Pembiayaan</option>
                                @foreach(\App\Models\PembiayaanModel::all() as $pembiayaan)
                                    <option value="{{ $pembiayaan->id }}">{{ $pembiayaan->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Pasien</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nama" placeholder="Nama Pasien" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">NIK</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nik" placeholder="NIK" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">No BPJS</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="no_bpjs" placeholder="No BPJS" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">No KK</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="no_kk" placeholder="No KK" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Nama KK</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nama_kk" placeholder="Nama KK" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Tanggal Lahir</label>
                            <input type="date" class="form-control my-2" id="exampleInputText1" name="tanggal_lahir" placeholder="Tanggal Lahir" required>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                            <select id="select-jenisKel" class="form-control Select2 form-control-lg" name="jenis_kelamin" aria-label="Default select example">
                                <option value="">Pilih Jenis Kelamin</option>
                                <option value="1">Laki-Laki</option>
                                <option value="2">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Golongan Darah</label>
                            <select id="select-jenisKel" class="form-control Select2 form-control-lg" name="golongan_darah" aria-label="Default select example">
                                <option value="">Pilih Golongan Darah</option>
                                <option value="O">O</option>
                                <option value="B">B</option>
                                <option value="A">A</option>
                                <option value="AB">AB</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Pendidikan</label>
                            <select id="select-jenisKel" class="form-control Select2 form-control-lg" name="pendidikan" aria-label="Default select example">
                                <option value="">Pilih Pendidikan</option>
                                <option value="Belum Sekolah">Belum Sekolah</option>
                                <option value="SD">SD</option>
                                <option value="SMP">SMP</option>
                                <option value="SMA/SMK">SMA/SMK</option>
                                <option value="D1">D1</option>
                                <option value="D2">D2</option>
                                <option value="D3">D3</option>
                                <option value="D4/S1">D4/S1</option>
                                <option value="S2">S2</option>
                                <option value="S3">S3</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Pekerjaan</label>
                            <select id="select-jenisKel" class="form-control Select2 form-control-lg" name="pekerjaan" aria-label="Default select example">
                                <option value="">Pilih Pekerjaan</option>
                                <option value="Tidak Bekerja">Tidak Bekerja</option>
                                <option value="PNS">PNS</option>
                                <option value="BUMN">BUMN</option>
                                <option value="Karyawan Swasta">Karyawan Swasta</option>
                                <option value="Wiraswasta">Wiraswasta</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Provinsi</label>
                            <select id="select-provinsi" class="form-control Select2 form-control-lg" name="m_provinsi_id" aria-label="Default select example">
                                <option value="">Pilihan Provinsi Sedang di Proses</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Daerah</label>
                            <select id="select-daerah" class="form-control Select2 form-control-lg" name="m_daerah_id" aria-label="Default select example">
                                <option value="">Pilih Provinsi Terlebih Dahulu</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Kecamatan</label>
                            <select id="select-kecamatan" class="form-control Select2 form-control-lg" name="m_kecamatan_id" aria-label="Default select example">
                                <option value="">Pilih Daerah Terlebih Dahulu</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Desa</label>
                            <select id="select-desa" class="form-control Select2 form-control-lg" name="m_desa_id" aria-label="Default select example">
                                <option value="">Pilih Desa Terlebih Dahulu</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Alamat</label>
                            <textarea class="form-control my-2" name="alamat" id="exampleFormControlTextarea1" rows="5"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleInputText1">RT</label>
                                    <input type="text" class="form-control my-2" id="exampleInputText1" name="rt" placeholder="RT" required>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleInputText1">RW</label>
                                    <input type="text" class="form-control my-2" id="exampleInputText1" name="rw" placeholder="RW" required>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleInputText1">Kode Pos</label>
                                    <input type="text" class="form-control my-2" id="exampleInputText1" name="kodepos" placeholder="Kode Pos" required>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary me-1 mt-2">Submit</button>
                        <button type="reset" class="btn iq-bg-danger mt-2">Cancel</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-library')
    {{-- Tempat Ngoding Meletakkan js library --}}
@endsection

@section('js-custom')
    {{-- Tempat Ngoding Meletakkan js custom --}}
    <script>
        $(function() {
            $("#isi-tabel").DataTable(); // tambahin ini ki

            let baseUrl     = '{{ url('') }}';
            $('#tabel-jquery').hide();

            $.getJSON(baseUrl+'/api/getAllProvinsi', (result) => {
                if (result.error_code == '0') {
                    let opt = '<option value="">- Pilih -</option>';
                    $.each(result.data, function(i, item) {
                        opt += '<option value="'+item.id+'">'+item.nama+'</option>';
                    });
                    $("#select-provinsi").html(opt);
                }else{
                    let opt = '<option value="">- Tidak Bisa -</option>';
                    $("#select-provinsi").html(opt);
                }
                $("#select-provinsi").select2({
                    placeholder: "Pilih Provinsi",
                });
            });

            $('#select-provinsi').on('change', function() {
                var val1 = this.value;
                $.getJSON(baseUrl+'/api/getDaerah/'+val1, (result) => {
                    if (result.error_code == '0') {
                        let opt = '<option value="">- Pilih Daerah -</option>';
                        // var dataSem = result.data;
                        // dataSem.sort(function(a, b){
                        //     return a.id_semester - b.id_semester;
                        // });
                        $.each(result.data, function(i, item) {
                            opt += '<option value="'+item.id+'">'+item.nama+'</option>';
                        });
                        $("#select-daerah").html(opt);
                    }else{
                        let opt = '<option value="">- Tidak Bisa -</option>';
                        $("#select-daerah").html(opt);
                    }
                    $("#select-daerah").select2({
                        placeholder: "Pilih Daerah",
                    });
                });
            });

            $('#select-daerah').on('change', function() {
                var val1 = this.value;
                $.getJSON(baseUrl+'/api/getKecamatan/'+val1, (result) => {
                    if (result.error_code == '0') {
                        let opt = '<option value="">- Pilih Kecamatan -</option>';
                        // var dataSem = result.data;
                        // dataSem.sort(function(a, b){
                        //     return a.id_semester - b.id_semester;
                        // });
                        $.each(result.data, function(i, item) {
                            opt += '<option value="'+item.id+'">'+item.nama+'</option>';
                        });
                        $("#select-kecamatan").html(opt);
                    }else{
                        let opt = '<option value="">- Tidak Bisa -</option>';
                        $("#select-kecamatan").html(opt);
                    }
                    $("#select-kecamatan").select2({
                        placeholder: "Pilih Kecamatan",
                    });
                });
            });

            $('#select-kecamatan').on('change', function() {
                var val1 = this.value;
                $.getJSON(baseUrl+'/api/getDesa/'+val1, (result) => {
                    if (result.error_code == '0') {
                        let opt = '<option value="">- Pilih Desa/Kelurahan -</option>';
                        // var dataSem = result.data;
                        // dataSem.sort(function(a, b){
                        //     return a.id_semester - b.id_semester;
                        // });
                        $.each(result.data, function(i, item) {
                            opt += '<option value="'+item.id+'">'+item.nama+'</option>';
                        });
                        $("#select-desa").html(opt);
                    }else{
                        let opt = '<option value="">- Tidak Bisa -</option>';
                        $("#select-desa").html(opt);
                    }
                    $("#select-desa").select2({
                        placeholder: "Pilih Desa/Kelurahan",
                    });
                });
            });

        });
    </script>
@endsection