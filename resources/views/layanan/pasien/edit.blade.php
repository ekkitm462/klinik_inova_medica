@extends('layouts.app')

@section('title', 'Edit Data Pasien')

@section('css-library')
    {{-- Tempat Ngoding Meletakkan css library --}}
@endsection

@section('css-custom')
    {{-- Tempat Ngoding Meletakkan css custom --}}
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="iq-card">
                <div class="iq-card-header d-flex justify-content-between">
                   <div class="iq-header-title">
                      <h4 class="card-title">{{ __('Edit Data Pasien') }}</h4>
                   </div>
                </div>
                <div class="iq-card-body">
                    <div class="btn-group">
                        <a href="{{ route('pasien') }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Tambah Unit">
                            Kembali
                        </a>
                    </div>
                    <div class="btn-group">&nbsp;</div>
                   <form action="{{ route('pasien.update', $pasien) }}" method="POST">
                    @csrf
                    @method('put')
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Pasien</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nama" value="{{ old('nama', $pasien->nama) }}" required>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-form-label">Jenis Pembiayaan</label>
                            <select id="select-jenis-pembiayaan" class="form-control Select2 form-control-lg" name="m_pembiayaan_id" aria-label="Default select example">
                                <option value="{{ $pasien->pembiayaan->id }}">{{ $pasien->pembiayaan->nama }}</option>
                                <option value="">Pilih Jenis Pembiayaan</option>
                                @foreach(\App\Models\PembiayaanModel::all() as $pembiayaan)
                                    <option value="{{ $pembiayaan->id }}">{{ $pembiayaan->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">NIK</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nik" value="{{ old('nik', $pasien->nik) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">No BPJS</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="no_bpjs" value="{{ old('no_bpjs', $pasien->no_bpjs) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">No KK</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="no_kk" value="{{ old('no_kk', $pasien->no_kk) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Nama KK</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" name="nama_kk" value="{{ old('nama_kk', $pasien->nama_kk) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Tanggal Lahir</label>
                            <input type="date" class="form-control my-2" id="exampleInputText1" name="tanggal_lahir" value="{{ old('tanggal_lahir', $pasien->tanggal_lahir) }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Provinsi</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" value="{{ old('m_provinsi_id', $pasien->provinsi->nama) }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Daerah</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" value="{{ old('m_daerah_id', $pasien->daerah->nama) }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Kecamatan</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" value="{{ old('m_kecamatan_id', $pasien->kecamatan->nama) }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Nama Desa/Kelurahan</label>
                            <input type="text" class="form-control my-2" id="exampleInputText1" value="{{ old('m_desa_id', $pasien->desa->nama) }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputText1">Alamat</label>
                            <textarea class="form-control my-2" name="alamat" id="exampleFormControlTextarea1" rows="5">{{ old('alamat', $pasien->alamat) }}</textarea>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleInputText1">RT</label>
                                    <input type="text" class="form-control my-2" id="exampleInputText1" name="rt" value="{{ old('rt', $pasien->rt) }}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleInputText1">RW</label>
                                    <input type="text" class="form-control my-2" id="exampleInputText1" name="rw" value="{{ old('rw', $pasien->rw) }}">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="exampleInputText1">Kode Pos</label>
                                    <input type="text" class="form-control my-2" id="exampleInputText1" name="kodepos" value="{{ old('kodepos', $pasien->kodepos) }}">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary me-1 mt-2">Submit</button>
                        <button type="reset" class="btn iq-bg-danger mt-2">Cancel</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-library')
    {{-- Tempat Ngoding Meletakkan js library --}}
@endsection

@section('js-custom')
    {{-- Tempat Ngoding Meletakkan js custom --}}
@endsection