
<!doctype html>
<html lang="en" dir="ltr">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Login || {{ config('app.name') }}</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="{{ url('') }}/assets/images/favicon.ico" />
      <!-- Bootstrap CSS -->
      <link id="bootstrap-css" rel="stylesheet" href="{{ url('') }}/assets/css/bootstrap.min.css">
      <!-- Typography CSS -->
      <link rel="stylesheet" href="{{ url('') }}/assets/css/typography.css">
      <!-- Style CSS -->
      <link rel="stylesheet" href="{{ url('') }}/assets/css/style.css">
      <!-- Style-Rtl CSS -->
      <link rel="stylesheet" href="{{ url('') }}/assets/css/style-rtl.css">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="{{ url('') }}/assets/css/responsive.css">
   </head>
   <body>
      <!-- loader Start -->
      <div id="loading">
         <div id="loading-center">
         </div>
      </div>
      <!-- loader END -->
        <!-- Sign in Start -->
        <section class="sign-in-page">
            <div class="container sign-in-page-bg mt-5 mb-md-5 mb-0 p-0">
                <div class="row no-gutters">
                    <div class="col-md-6 text-center">
                        <div class="sign-in-detail text-white">
                            <a class="sign-in-logo mb-5" href="{{ url('') }}"><img src="{{ url('') }}/assets/images/logo-white.png" class="img-fluid" alt="logo"></a>
                            <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="false" data-dots="true" data-items="1" data-items-laptop="1" data-items-tab="1" data-items-mobile="1" data-items-mobile-sm="1" data-margin="0">
                                <div class="item">
                                    <img src="{{ url('') }}/assets/images/login/1.png" class="img-fluid mb-4" alt="logo">
                                    <h4 class="mb-1 text-white">PT Inova Medika Solusindo</h4>
                                    <p>Aplikasi Sistem Informasi Klinik</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 position-relative">
                        <div class="sign-in-from">
                            <h1 class="mb-0">Login</h1>
                            <p>Silahkan Masukkan Emial dan Password!</p>
                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Error!</strong>
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}
                                    @endforeach
                                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <form class="mt-4" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputEmail1" class="mb-2">Email</label>
                                    <input type="email" class="form-control mb-0" id="exampleInputEmail1" name="email" placeholder="Masukkan Email">
                                </div>
                                <div class="d-flex justify-content-between my-2">
                                    <label for="exampleInputPassword1">Password</label>
                                    <a href="{{ url('') }}" class="float-end">Forgot password?</a>
                                </div>
                                <input type="password" class="form-control mb-0" id="exampleInputPassword1" name="password" placeholder="Password">
                                <div class="d-flex w-100 justify-content-between  align-items-center mt-3 w-100">
                                    <div class="custom-control custom-checkbox d-inline-block mt-2 pt-1">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Remember Me</label>
                                    </div>
                                    <button type="submit" class="btn btn-primary float-end">Sign in</button>
                                </div>
                                <div class="sign-info">
                                    <span class="dark-color d-inline-block line-height-2">Belum punya akun? <a href="sign-up.html">Register!</a></span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Sign in END -->
      <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="{{ url('') }}/assets/js/jquery.min.js"></script>
      <script src="{{ url('') }}/assets/js/popper.min.js"></script>
      <script src="{{ url('') }}/assets/js/bootstrap.min.js"></script>
      <!-- Appear JavaScript -->
      <script src="{{ url('') }}/assets/js/jquery.appear.js"></script>
      <!-- Countdown JavaScript -->
      <script src="{{ url('') }}/assets/js/countdown.min.js"></script>
      <!-- Counterup JavaScript -->
      <script src="{{ url('') }}/assets/js/waypoints.min.js"></script>
      <script src="{{ url('') }}/assets/js/jquery.counterup.min.js"></script>
      <!-- Wow JavaScript -->
      <script src="{{ url('') }}/assets/js/wow.min.js"></script>
      <!-- Apexcharts JavaScript -->
      <script src="{{ url('') }}/assets/js/apexcharts.js"></script>
      <!-- Swiper Slider JavaScript -->
      <script src="{{ url('') }}/assets/js/swiper-bundle.min.js"></script>
      <!-- Select2 JavaScript -->
      <script src="{{ url('') }}/assets/js/select2.min.js"></script>
      <!-- Owl Carousel JavaScript -->
      <script src="{{ url('') }}/assets/js/owl.carousel.min.js"></script>
      <!-- Magnific Popup JavaScript -->
      <script src="{{ url('') }}/assets/js/jquery.magnific-popup.min.js"></script>
      <!-- Smooth Scrollbar JavaScript -->
      <script src="{{ url('') }}/assets/js/smooth-scrollbar.js"></script>
      <!-- Chart Custom JavaScript -->
      <script src="{{ url('') }}/assets/js/chart-custom.js"></script>
      <!-- Custom JavaScript -->
      <script src="{{ url('') }}/assets/js/custom.js"></script>
   </body>
</html>